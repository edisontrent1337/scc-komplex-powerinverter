/**
 * Copyright (c) 2010-2019 by the respective copyright holders.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 */
package org.openhab.binding.powerinverter.internal;

import org.eclipse.jdt.annotation.NonNullByDefault;
import org.eclipse.smarthome.core.thing.ThingTypeUID;

/**
 * The {@link PowerInverterBindingConstants} class defines common constants, which are
 * used across the whole binding.
 *
 * @author Team KomplexKiwi - Initial contribution
 */
@NonNullByDefault
public class PowerInverterBindingConstants {

    private static final String BINDING_ID = "powerinverter";

    // List of all Thing Type UIDs
    public static final ThingTypeUID THING_TYPE_SAMPLE = new ThingTypeUID(BINDING_ID, "inverter");

    // List of all Channel ids
    public static final String EXTERIOR_LIGHTING_SWITCH = "exteriorLighting";
    public static final String CAMPING_PLUG_SWITCH = "campinglotPlug";
    public static final String EXTERIOR_LIGHTING_CURRENT_WORK = "exteriorLightingCurrentWork";
    public static final String CAMPING_PLUG_CURRENT_WORK = "campinglotPlugCurrentWork";
    public static final String CAMPING_PLUG_WORK_THIS_DAY = "campinglotWorkThisDay";

    public static final String EXTERIOR_LIGHTING_START_TIME = "exteriorLightingStartTime";
    public static final String EXTERIOR_LIGHTING_END_TIME = "exteriorLightingEndTime";
    public static final String MAXIMUM_PLUG_WORK_PER_DAY = "maximumPlugWorkPerDay";
    public static final String MINIMUM_BATTERY_STATE_OF_CHARGE = "minimumBatteryStateOfCharge";
}
